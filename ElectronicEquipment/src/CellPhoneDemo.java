/*
 	Call CellPhones class and create object phone
 	Output info about cell phones compared
 	
 	run phone object
 	ask user to continue or to end program
 */

import java.util.Scanner;
public class CellPhoneDemo 
{
	public static void main(String[] args)
	{
		CellPhones phone = new CellPhones();
		String question;
		
		System.out.println("Cell Phone Comparison. iPhone 5s vs. HTC Droid vs. Galaxy 5s.");
		System.out.println("There is information about screen size, battery life, and cost.");
		
		do
	    {
			phone.getAttribute();
			phone.getSpecification();
			phone.writeOutput();
		
			System.out.println("Do you want to look up another specification? If so, enter <yes>.");
			Scanner keyboard = new Scanner(System.in);
			question = keyboard.nextLine();
		
			if (!question.equalsIgnoreCase("yes"))
			{
				System.out.print("Good Bye\n");
				System.exit(0);
			}
			
	    } while (question.equalsIgnoreCase("yes"));
		
	}
}
