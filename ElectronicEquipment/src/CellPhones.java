import java.util.Scanner;
public class CellPhones 
{
	public String phone;
	public String attribute;
	public String batteryLife;
	public String cost;
	
	public String iPhoneSpec;
	public String droidSpec;
	public String galaxySpec;
	
	public void getAttribute()
	{
		int attributeChosen;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("What would you like to look up?");
		System.out.println("Enter <1> for screen size, <2> for battery life, or <3> for cost.");
		attribute = keyboard.nextLine();
		
		if ((attribute.equals("1")) || (attribute.equals("2")) || (attribute.equals("3")))
		{
			attributeChosen = Integer.parseInt(attribute);
			
			if (attributeChosen == 1)
				attribute = ("Screen Size");
			else if (attributeChosen == 2)
				attribute = ("Battery Life");
			else
				attribute = ("Cost");
		}
		else
		{	
			keyboard = new Scanner(System.in);
			System.out.println("Please enter <1> for screen size, <2> for battery life, or <3> for cost.");
			attribute = keyboard.nextLine();
			
			while ((!attribute.equals("1")) && (!attribute.equals("2")) && (!attribute.equals("3")))
			{
				keyboard = new Scanner(System.in);
				System.out.println("Enter <1> for screen size, <2> for battery life, or <3> for cost.");
				attribute = keyboard.nextLine();
			}
			
			attributeChosen = Integer.parseInt(attribute);
			
			if (attributeChosen == 1)
				attribute = ("Screen Size");
			else if (attributeChosen == 2)
				attribute = ("Battery Life");
			else
				attribute = ("Cost");
		}
		
	}
	
	public void getSpecification()
	{
		if (attribute.equals("Screen Size"))
		{
			iPhoneSpec = ("4 in.");
			droidSpec = ("4 in.");
			galaxySpec = ("5.1 in.");
		}
		else if (attribute.equals("Battery Life"))
		{
			iPhoneSpec = ("10 hours");
			droidSpec = ("9 hours");
			galaxySpec = ("29 hours");
		}
		else
		{
			iPhoneSpec = ("$199 (16 GB)");
			droidSpec = ("$349 (8 GB)");
			galaxySpec = ("$199 (16 GB)");
		}
	}
	
	public void writeOutput()
	{
		
		System.out.println("Specification: " + attribute);
		System.out.println("iPhone: " + iPhoneSpec);
		System.out.println("Droid: " + droidSpec);
		System.out.println("Galaxy: " + galaxySpec);
	}
}
